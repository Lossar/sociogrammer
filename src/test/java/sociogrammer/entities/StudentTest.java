package sociogrammer.entities;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class StudentTest {
  Student stud;
  //Storing the name as a string to ensure deterministic result among tests when checking name
  String testStudName;
  
  /**
   * Sets up data for tests
   */
  @BeforeEach
  void setStud() {
    testStudName = "Lorem";
    stud = new Student(testStudName);
  }
  
  /**
   * Ensures objects are created when provided valid parameters
   */
  @Test
  void testStudentConstructor() {
    Student student = new Student("Lorem");
    assertNotNull(student);
  }
  
  /**
   * Tests that objects will not be created with invalid name parameters
   */
  @Test
  void testInvalidNameParameterInConstructor() {
    testStudName = "";
    try{
      stud = new Student(testStudName);
      fail(); //Fails if no error is thrown
    } catch (IllegalArgumentException e) {
      //Test passes if this line is reached
    }
  }
  
  /**
   * Tests that objects will not be created with null as name parameter for students
   */
  @Test
  void testNullNameParameterInConstructor() {
    try{
      stud = new Student(null);
      fail();
    } catch (IllegalArgumentException e) {
      //Test passes if this line is reached
    }
  }
  
  /**
   * Tests the <code>getName</code> method, ensures correct name is returned
   */
  @Test
  void testGetName() {
    assertEquals(testStudName, stud.getName());
  }
  
  /**
   * Test setting of new name of an already existing student.
   */
  @Test
  void setName() {
    testGetName(); //testGetName is ran as it is a prerequisite for this test to be accurate
    String name = "Ipsum";
    stud.setName(name);
    assertEquals(name, stud.getName());
  }
  
}