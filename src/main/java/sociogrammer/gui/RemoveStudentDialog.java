package sociogrammer.gui;

import java.util.Optional;
import java.util.Set;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import sociogrammer.entities.Student;

public class RemoveStudentDialog extends Dialog<Student> {
  private ListView<Student> students;
  ButtonType removeButton = null;
  
  public RemoveStudentDialog(Set<Student> students) {
    this.initialize();
    this.students.setItems(FXCollections.observableArrayList(students));
    this.setResultConverter(e -> {
      Student selectedStudent = this.students.getSelectionModel().getSelectedItem();
      if (e.equals(removeButton)
      && selectedStudent != null){
        return selectedStudent;
      } else {
        return null;
      }
    });
  }
  
  private void initialize() {
    this.setHeaderText("Remove Student");
    this.setTitle("Remove student");
    this.setContentText("Please specify which student you wish to remove");
    
    removeButton = new ButtonType("Remove");
    ButtonType cancelButton = new ButtonType("Cancel");
    System.out.println(removeButton.getButtonData());
    
    this.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE, removeButton);
    
    students = new ListView<>();
    this.getDialogPane().setContent(students);
  }
}
