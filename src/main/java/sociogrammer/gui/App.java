package sociogrammer.gui;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class of the application. Contains the main methods responsible for starting the application
 *
 * @author Lars Ivar Ramberg
 * @version 24.05.2020
 */
public class App extends Application {
  private static Scene scene;
  
  /**
   * Gets the stage of this Application.
   * @return the current stage of the application
   */
  public static Stage getStage() {
    return stage;
  }
  
  private static Stage stage;

  @Override
  public void start(Stage stage) throws IOException {
    setStartScene(stage);
    App.stage = stage;
    stage.setTitle("Sociogrammer");
    stage.show();
  }
  
  /**
   * Sets the first scene.
   * @throws IOException if the fxml fails to load
   */
  private static void setStartScene(Stage stage) throws IOException {
    scene = new Scene(loadFxml("startscreen"));
    stage.setScene(scene);
  }
  
  /**
   * Sets the root of scene.
   * @param fxml fxml file to set as root
   * @throws IOException if the specified fxml file is not found
   */
  static void setRoot(String fxml) throws IOException {
    scene.setRoot(loadFxml(fxml));
  }
  
  /**
   * Loads an fxml file and uses it as the current fxml for the application's stage.
   * @param fxml name of fxml file
   * @return parent with the specified fxml file as its configuration
   * @throws IOException if the fxml file is not found
   */
  public static Parent loadFxml(String fxml) throws IOException {
    FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/sociogrammer/"+fxml + ".fxml"));
    return fxmlLoader.load();
  }
  
  /**
   * Main method of the application.
   * @param args arguments for execution. Not used
   */
  public static void main(String[] args) {
    launch(args);
  }
  
  /**
   * Sets the lower bounds of the application in width and height.
   * @param x minimum width of the application
   * @param y minimum height of the application
   */
  public static void setLowerBounds(int x, int y) {
    stage.close();
    stage.setMinWidth(x);
    stage.setMinHeight(y);
    App.setUpperBounds(x,y);
    stage.show();
  }
  
  /**
   * Sets the lower bounds of the application in width and height.
   * @param x minimum width of the application
   * @param y minimum height of the application
   */
  public static void setUpperBounds(int x, int y) {
    stage.setMaxWidth(x);
    stage.setMaxHeight(y);
  }
}