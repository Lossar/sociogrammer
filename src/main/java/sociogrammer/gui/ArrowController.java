package sociogrammer.gui;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import javafx.scene.Group;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import sociogrammer.entities.Arrow;
import sociogrammer.entities.Student;

/**
 * Objects of the arrow controller class represents a controller for active arrows in a sociogram.
 * The controller wil create and manage all new arrows between students, as well as draw arrows as
 * appropriate.
 *
 * @author Lars Ivar Ramberg
 * @version 09.06.2020
 */
public class ArrowController{
  private static final float ARROW_LENGTH = 20;
  private static final float ARROW_WIDTH = 7;
  private HashSet<Arrow> arrows;
  private Arrow currentArrow;
  private Group currentArrowGui;
  private ArrayList<Group> arrowGuis;
  private boolean drawing;
  private Pane rootPane;
  
  /**
   * Default constructor for arrow controllers.
   */
  public ArrowController(Pane rootPane) {
    this.rootPane = rootPane;
    this.drawing = false;
    arrows =  new HashSet<>();
    arrowGuis = new ArrayList<>();
    rootPane.setOnMouseMoved(e -> {
      if(drawing) {
        this.updateArrow((float)e.getSceneX(), (float)e.getSceneY(), this.currentArrowGui, this.currentArrow);
        System.out.println(e.getSceneX() + " " + e.getSceneY());
      }
    });
  }
  
  /**
   * Sets the current arrow.
   * @param currentArrow arrow to be set as current
   */
  private void setCurrentArrow(Arrow currentArrow) {
    this.currentArrow = currentArrow;
  }
  
  /**
   * Handles the current action on a student, and creates or ends an arrow as appropriate.
   * @param actionTarget target of action
   * @param x position of action in the x-axis
   * @param y position of action in the y-axis
   */
  public void handleArrowAction(Student actionTarget, float x, float y) {
    if (this.currentArrow == null) {
      this.startNewArrow(actionTarget, x, y);
    } else if (currentArrow.getStartingStudent().equals(actionTarget)) {
      this.removeArrow(currentArrowGui);
      restartArrow();
    } else {
      completeArrow(actionTarget, x, y);
      restartArrow();
    }
  }
  
  public void cancelArrow() {
    this.removeArrow(currentArrowGui);
    restartArrow();
  }
  
  private void restartArrow() {
    currentArrow = null;
    currentArrowGui = null;
    drawing = false;
  }
  /**
   * Starts a new arrow.
   * @param x point of start in the x axis
   * @param y point of start in the y axis
   * @param startingStudent student at the starting point
   */
  public void startNewArrow(Student startingStudent, float x, float y) {
    drawing = true;
    Arrow arrow =  new Arrow(startingStudent, x, y);
    this.setCurrentArrow(arrow);
    currentArrowGui = createArrowGui(x, y);
    currentArrowGui.setMouseTransparent(true);
    rootPane.getChildren().add(currentArrowGui);
  }
  
  /**
   * Initializes the GUI for a new arrow, and sets the current arrowGUI to be this newly created
   * arrow.
   * @param x starting x of the new arrow
   * @param y starting y of the new arrow
   * @return Group consisting of three lines, creating an arrow
   */
  public Group createArrowGui(float x, float y) {
    Arrow.ArrowCoordinate coordinate = currentArrow.getStartCoordinate();
    Line arrow = new Line(coordinate.getxCord(),coordinate.getyCord(),x,y);
    Line arrow1 = new Line();
    Line arrow2 = new Line();
    arrow.setStrokeWidth(3);
    arrow1.setStrokeWidth(2);
    arrow2.setStrokeWidth(2);
    
    this.arrows.add(this.currentArrow);
    
    Group completeArrow = new Group();
    completeArrow.getChildren().add(arrow);
    completeArrow.getChildren().add(arrow1);
    completeArrow.getChildren().add(arrow2);
    completeArrow.setUserData(this.currentArrow);
    this.arrowGuis.add(completeArrow);
    completeArrow.setUserData(this.currentArrow);
    
    return completeArrow;
  }
  
  /**
   * Updates the ending position of an arrow
   * @param x
   * @param y
   */
  private void updateArrow(float x, float y, Group arrowToUpdate, Arrow arrowData) {
    Line arrow = (Line)arrowToUpdate.getChildren().get(0);
    Line arrow1 = (Line)arrowToUpdate.getChildren().get(1);
    Line arrow2 = (Line)arrowToUpdate.getChildren().get(2);
    
    double sx = arrowData.getStartCoordinate().getxCord();
    double sy = arrowData.getStartCoordinate().getyCord();
    double ex = x;
    double ey = y;
  
    arrow.setEndX(x);
    arrow.setEndY(y);
    arrow1.setEndX(ex);
    arrow1.setEndY(ey);
    arrow2.setEndX(ex);
    arrow2.setEndY(ey);
  
  
    if (ex == sx && ey == sy) {
      // arrow parts of length 0
      arrow1.setStartX(ex);
      arrow1.setStartY(ey);
      arrow2.setStartX(ex);
      arrow2.setStartY(ey);
    } else {
      double factor = ARROW_LENGTH / Math.hypot(sx -ex, sy - ey);
      double factorO = ARROW_WIDTH / Math.hypot(sx- ex, sy - ey);
    
      // part in direction of main line
      double dx = (sx - ex) * factor;
      double dy = (sy - ey) * factor;
    
      // part ortogonal to main line
      double ox = (sx - ex) * factorO;
      double oy = (sy - ey) * factorO;
    
      arrow1.setStartX(ex + dx - oy);
      arrow1.setStartY(ey + dy + ox);
      arrow2.setStartX(ex + dx + oy);
      arrow2.setStartY(ey + dy - ox);
    }
  }
  /**
   * Ends the current arrow, and prepares for a new arrow.
   * @param endStudent student at the tip (end) of the arrow
   * @param x x coordinate for arrow tip
   * @param y coordinate for arrow tip in the y axis
   */
  public Group completeArrow(Student endStudent, float x, float y) {
    currentArrow.setEndingStudent(endStudent, x, y);
    this.updateArrow(x, y, currentArrowGui, currentArrow);
    
    Group arrowToRemove = this.currentArrowGui;
    currentArrowGui.setOnMouseClicked(event -> {
      if(event.getButton() == MouseButton.SECONDARY) {
        removeArrow(arrowToRemove);
      }
    });
    currentArrowGui.setMouseTransparent(false);
    return currentArrowGui;
  }
  
  /**
   * Removes an arrow and its GUI representation.
   * @param arrow arrow to remove
   */
  private void removeArrow(Group arrow) {
    //TODO need to link guis and arrowdata, so that appropriate data is removed.
    arrows.remove(currentArrow);
    arrowGuis.remove(arrow);
    rootPane.getChildren().remove(arrow);
  }
  /**
   * Resets all arrows, deleting them and removing them from the GUI.
   */
  public void resetArrows() {
    Iterator<Group> arrIt = arrowGuis.iterator();
    while(arrIt.hasNext()) {
      rootPane.getChildren().remove(arrIt.next());
      arrIt.remove();
    }
    this.arrows.clear();
  }
  
  public void removeByName(String name) {
    ArrayList<Group> removables = new ArrayList<>();
    this.arrowGuis.forEach(arr -> {
      Arrow arrow = (Arrow)arr.getUserData();
      String[] names = arrow.getStudentNames();
      if(name.equals(names[0]) || name.equals(names[1])) {
        removables.add(arr);
      }
    });
    removables.forEach(this::removeArrow);
  }
  
  public boolean isDrawing (){
    return this.drawing;
  }
}
