package sociogrammer.gui;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import sociogrammer.entities.Student;
import sociogrammer.util.FileUtil;
import sociogrammer.util.ScreenCaptureUtil;

/**
 * Controller for primary screen (sociogram view).
 *
 * @author Lars Ivar Ramberg
 * @version 24.05.2020
 */
public class PrimaryController {
  
  @FXML
  private Label nameLabel;
  @FXML
  private GridPane centerGrid;
  @FXML
  private MenuItem fileNew, takeScreenshot, editAddStudent, editRemoveStudent;
  @FXML
  private Pane rootPane;
  @FXML
  private MenuBar menuBar;
  @FXML
  private ImageView centerTable;
  private ArrayList<Button> studentButtons;
  private ArrayList<VBox> studentContainers;
  private ArrowController arrowController;
  private HashSet<Student> students;
  private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
  
  @FXML
  private void switchToSecondary() throws IOException {
    App.setRoot("secondary");
  }
  
  /**
   * Initializes this controller. Initialize is called when the appropriate FXML is loaded.
   */
  @FXML
  public void initialize() {
    this.studentButtons = this.findInitialButtons();
    this.students = new HashSet<>();
    this.studentContainers = new ArrayList<>();
    this.arrowController = new ArrowController(this.rootPane);
    fileNew.setOnAction(e -> createNewSociogram());
    takeScreenshot.setOnAction(e -> takeScreenshot());
    this.createNewSociogram();
    App.setLowerBounds(800, 800);
    App.setUpperBounds(800,800);
    
    this.editAddStudent.setOnAction(e -> this.doAddChildrenByMenuClick());
    this.editRemoveStudent.setOnAction(e -> this.doRemoveStudentByMenuClick());
    
    //temporary solution, could be improved by somehow "tagging" the appropriate classes 
    this.rootPane.setOnMouseClicked(e-> {
      if(e.getTarget().getClass() != VBox.class
        && e.getTarget().getClass() != ImageView.class) {
        this.arrowController.cancelArrow();
      }
    });
    Student tableStudent = new Student("Group as a whole");
    this.centerTable.setOnMouseClicked(e -> {
      handleClickOnStudent(tableStudent, e);
    });
  }
  
  private void takeScreenshot() {
    Optional<File> fileToSave = FileUtil.doGetNewFileFromUser();
    if(fileToSave.isPresent()) {
      menuBar.setVisible(false);
      ScreenCaptureUtil.captureScreen(rootPane, fileToSave.get());
    } else {
      logger.log(Level.WARNING, "User tried to create invalid file");
      //TODO refactor into seperate method and/or class if necessary
      Alert alert = new Alert(Alert.AlertType.WARNING, "Window for saving sociogram " +
          "was closed before completion. File was not created");
      alert.setTitle("File aborted");
      alert.setHeaderText("File creation aborted");
      alert.showAndWait();
    }
    menuBar.setVisible(true);

  }
  /**
   * Creates a new sociogram and resets the current one.
   */
  private void createNewSociogram() {
    this.doShowNameSociogramDialog();
    this.resetSociogram();
  }
  
  /**
   * Resets the student elements of the sociogram and replaces them with buttons.
   */
  private void resetSociogram() {
    if (studentContainers.size() > 0) {
      this.arrowController.resetArrows();
      Iterator<VBox> containerIterator = studentContainers.iterator();
      while (containerIterator.hasNext()) {
        VBox studentContainer = containerIterator.next();
        try {
          Button newButton = this.loadAndPrimeAddStudentButtonButton();
          this.replaceElement(newButton, studentContainer);
        } catch (IOException e) {
          logger.log(Level.SEVERE,"Button could not be loaded");
        } finally {
          containerIterator.remove();
        }
      }
    }
  }
  
  /**
   * Loads a button and sets the addstudent function to it.
   * @return button with add student as action event
   * @throws IOException if the fxml file is not found
   */
  private Button loadAndPrimeAddStudentButtonButton() throws IOException {
    Button newButton = (Button)App.loadFxml("addstudentbutton");
    newButton.setOnAction(event -> addStudent(newButton));
    return newButton;
  }
  
  /**
   * Shows a dialog for input of the name of the group about to be sociogramed, before
   * transiotioning into sociogram view. The transition is cancelled if the naming is cancelled.
   */
  @FXML
  private void doShowNameSociogramDialog() {
    TextInputDialog  textInputDialog = new TextInputDialog("Enter name of group");
    textInputDialog.setTitle("New sociogram");
    textInputDialog.setHeaderText("Please name the group for the sociogram");
    textInputDialog.setContentText("Name:");
    Optional<String> result          = textInputDialog.showAndWait();
    
    if (result.isPresent()) {
      this.nameLabel.setText(result.get());
      this.setButtons();
    } else {
      try {
        this.switchToSecondary();
      } catch (IOException e) {
        //TODO handle
      }
    }
  }
  
  /**
   * Finds an available add student button, simulates click, and creates a new student.
   * If no buttons are available, an error is presented to the user.
   */
  private void doAddChildrenByMenuClick() {
    if(!this.studentButtons.isEmpty()){
      this.studentButtons.get(0).fire();
    } else {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setHeaderText("No more room!");
      alert.setTitle("Out of room");
      alert.setContentText("There is no more room to add any more students! Please remove one or " +
                               "more students to add more.");
      alert.showAndWait();
    }
  }
  
  private void doRemoveStudentByMenuClick() {
    RemoveStudentDialog dialog = new RemoveStudentDialog(this.students);
    Optional<Student> toRemove = dialog.showAndWait();
    VBox guiToRemove = null;
    if(toRemove.isPresent()) {
      boolean found = false;
      Iterator<VBox> studGuiIterator = studentContainers.iterator();
      
      while (studGuiIterator.hasNext() && !found) {
        VBox currentContainer = studGuiIterator.next();
        if (currentContainer.getUserData().equals(toRemove.get())) {
          guiToRemove = currentContainer;
          found = true;
        }
      }
      
      try {
        this.removeStudent(guiToRemove);
      } catch (IOException e) {
        System.out.println("Something went terribly wrong!");
      }
    }

    
    //Show menu of available students to remove
    //Based on response from menu, either remove a student, or do not bother.
  }
  /**
   * Sets all the buttons to do desired action.
   */
  @FXML
  private void setButtons() {
    List<Button> buttons = getAllNodes(this.centerGrid)
        .stream()
        .filter(node -> node instanceof Button)
        .map(Button.class::cast).collect(Collectors.toList());
    buttons.forEach(button -> button.setOnAction(e -> addStudent(button)));
  }
  
  /**
   * Creates a new student object, and a fitting GUI representation provided that a valid name
   * is given by the user. If the user cancels the addition of a student or provides an
   * invalid name, no new student is created.
   * @param button element clicked to call this method
   */
  private void addStudent(Button button) {
    TextInputDialog nameDialog = new TextInputDialog();
    nameDialog.setTitle("Name student");
    nameDialog.setContentText("Name");
    nameDialog.setHeaderText("Name the new student");
    Optional<String> newStudentName = nameDialog.showAndWait();
    
    if (newStudentName.isPresent() &&
        !newStudentName.get().isEmpty() &&
        !newStudentName.get().isBlank()) {
      VBox studentGuiContainer = new VBox();
      Student student = new Student(newStudentName.get());
      this.students.add(student);
  
      try (InputStream is = PrimaryController.class
          .getClassLoader().getResourceAsStream("icons/avatar.png")) {
        
        if (is != null) {
          Image image = new Image(is);
          ImageView imageView = new ImageView(image);
  
          imageView.setFitWidth(50);
          imageView.setPreserveRatio(true);
  
          studentGuiContainer.setAlignment(Pos.CENTER);
          studentGuiContainer.getChildren().add(imageView);
          studentGuiContainer.getChildren().add(new Label(newStudentName.get()));
          studentGuiContainer.setOnMouseClicked(e -> this.handleClickOnStudent(student, e));
          studentGuiContainer.setUserData(student);
          this.studentButtons.remove(button);
          this.replaceElement(studentGuiContainer, button);
          this.studentContainers.add(studentGuiContainer);
        } else {
          throw new IllegalArgumentException("Input stream is null, the url is either wrong or"
                                                + "misspelled");
        }
      } catch (IOException e) {
        logger.severe("File was not found and/or could not be loaded.");
      } catch (IllegalArgumentException e) {
        logger.severe("Input stream is null..., path is likely miss-spelled");
      }
    }
  }
  
  private Student removeStudent(VBox studentGuiContainer) throws IOException {
    Button newButton = this.loadAndPrimeAddStudentButtonButton();
    Student studToRemove = (Student) studentGuiContainer.getUserData();
    this.studentContainers.remove(studentGuiContainer);
    this.students.remove(studToRemove);
    this.replaceElement(newButton, studentGuiContainer);
    this.arrowController.removeByName(studToRemove.getName());
    this.studentButtons.add(newButton);
    return studToRemove;
  }
  
  /**
   * Handles clicks when a student is clicked
   * @param student the student object clicked
   * @param event the click event of the click. "The click itself".
   */
  private void handleClickOnStudent(Student student, javafx.scene.input.MouseEvent event) {
    if(event.getButton() == MouseButton.PRIMARY) {
      arrowController.handleArrowAction(student, (float)event.getSceneX(), (float)event.getSceneY());
    } else if(event.getButton() == MouseButton.SECONDARY) {
      try {
        VBox target = null;
        if(event.getTarget().getClass() != VBox.class) {
          Node temp = (Node)event.getTarget();
            while(temp.getParent() != null && temp.getParent().getClass() != VBox.class) {
              temp = temp.getParent();
            }
          target = (VBox)temp.getParent();
        } else {
          target = (VBox)event.getTarget();
        }
        if(target != null && !this.arrowController.isDrawing()) {
          this.removeStudent(target);
        } else {
          this.arrowController.cancelArrow();
        }
      } catch (IOException e) {
        System.out.println(e);
      }
    }
  }
  
  /**
   * Replaces the <b>oldNode</b> with the <b>newNode</b> in the parent of the <b>oldNode</b>,
   * provided that the parent is a pane. If the parent is not a pane, an exception is thrown
   * @param newNode node to add to parent
   * @param oldNode node to remove from parent
   * @throws IllegalArgumentException if the parent of the oldNode is not a pane
   */
  private void replaceElement(Node newNode, Node oldNode) {
    Node parent = oldNode.getParent();
    if (!(parent instanceof Pane)) {
      throw new IllegalArgumentException("Parent of old note is not a pane! Replacement"
                                             + "could not be performed");
    }
    if(parent instanceof GridPane) {
      replaceElementInGridPane(newNode, oldNode, parent);
    } else {
      Pane parentPane = (Pane) parent;
      parentPane.getChildren().add(newNode);
      parentPane.getChildren().remove(oldNode);
    }

  }
  
  private void replaceElementInGridPane(Node newNode, Node oldNode, Node parent) {
    GridPane parentGrid = (GridPane) oldNode.getParent();
    parentGrid.getChildren().remove(oldNode);
    parentGrid.add(newNode, GridPane.getColumnIndex(oldNode), GridPane.getRowIndex(oldNode));
  }
  /**
   * Gets all children of the provided node.
   * @param root node to get child
   *             ren of
   * @return ArrayList containing all children of root node
   */
  private ArrayList<Node> getAllNodes(Parent root) {
    ArrayList<Node> nodes = new ArrayList<>();
    addAllDescendents(root, nodes);
    return nodes;
  }
  
  /**
   * Adds all decendant of a node, and checks its children for parenthood.
   * @param parent node to add to list of descendants
   * @param nodes list to add parents to
   */
  private void addAllDescendents(Parent parent, ArrayList<Node> nodes) {
    parent.getChildrenUnmodifiable()
          .forEach(node -> {
            nodes.add(node);
            if (node instanceof Parent) {
              addAllDescendents((Parent)node, nodes);
            }
          });
  }
  
  private Set<Node> findAllButtons() {
    return this.getAllNodes(this.rootPane).stream().filter(elem -> elem.getClass() == Button.class)
        .collect(Collectors.toSet());
  }
  
  private ArrayList<Button> findInitialButtons() {
    Set<Node> buttonsAsNodes = this.findAllButtons();
    ArrayList<Button> buttons = new ArrayList<>();
    buttonsAsNodes.forEach(e -> buttons.add((Button)e));
    return buttons;
  }
}
