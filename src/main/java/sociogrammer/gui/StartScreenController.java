package sociogrammer.gui;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * Controller for the start-screen window. Sets the window to the main view when appropriate.
 *
 * @author Lars Ivar Ramberg
 * @version 25.05.2020
 */
public class StartScreenController {
  
  @FXML
  private Button quit;
  @FXML
  private Button newSociogram;
  @FXML
  private Label titleLabel;
  private static final String version = getVersion();
  
  /**
   * Initializer for the controller. Sets required base-values and listeners for GUI interactions.
   */
  @FXML
  public void initialize() {
    this.titleLabel.setText("Sociogrammer, version: " + version);
    quit.setOnAction(event -> System.exit(0));
    newSociogram.setOnAction(e -> switchToSociogramView());
  }
  
  /**
   * Gets the current version of the application as a string.
   * @return the current version of the application, as a string.
   */
  private static String getVersion() {
    String version = "UNKNOWN";
    InputStream input = App.class.getClassLoader()
                                 .getResourceAsStream("config.properties");
    if (input != null) {
      Properties properties = new Properties();
      try {
        properties.load(input);
        version = properties.getProperty("version");
      } catch (IOException e) {
        //log.severe("Could not load version number. Properties file could not be loaded");
      }
    } else {
      //log.severe("Input stream for config.properties could not be properly created. Ensure"
       //              + "the argument for the input stream does not have typos and that the "
        //             + "properties file exists");
    }
    
    return version;
  }
  
  /**
   * Switches the view of the application to the sociogram view.
   */
  @FXML
  private void switchToSociogramView() {
    try {
      App.setRoot("primary");
    } catch (IOException e) {
      //Oops
    }
  }
  
}