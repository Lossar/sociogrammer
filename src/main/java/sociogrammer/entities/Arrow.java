package sociogrammer.entities;

import java.util.ArrayList;

/**
 * Objects of the arrow class represent arrows going between students in the sociogram.
 * The arrow needs two coordinates: one representing the point on the screen where the arrow starts,
 * and one representing where it ends. The arrow is also responsible for knowing which students it
 * connects
 *
 * @author Lars Ivar Ramberg
 * @version 08.06.2020
 */
public class Arrow {
  
  private Student startingStudent;
  private Student endingStudent;
  private ArrayList<ArrowCoordinate> coordinates;
  
  /**
   * Constructor for <b>Arrow</b> objects that takes a starting student, starting x position, and
   * ending y position.
   * @param startingStudent student who is at the root of the arrow
   * @param startX starting x position for the arrow
   * @param startY starting y position for the arrow
   */
  public Arrow(Student startingStudent, float startX, float startY) {
    if (startingStudent == null) {
      throw new IllegalArgumentException("Student may not be null");
    }
    this.coordinates = new ArrayList<>();
    this.startingStudent = startingStudent;
    ArrowCoordinate arrowCoordinate = new ArrowCoordinate(startX, startY);
    coordinates.add(arrowCoordinate);
  }
  
  /**
   * Gets the starting student of this arrow.
   * @return the starting student of this arrow
   */
  public Student getStartingStudent() {
    return startingStudent;
  }
  
  /**
   * Gets the start coordinate.
   * @return start coordinate of the arrow
   */
  public ArrowCoordinate getStartCoordinate() {
    ArrowCoordinate startCoordinate = null;
    if (this.coordinates.get(0) != null) {
      startCoordinate = this.coordinates.get(0);
    }
    return startCoordinate;
  }
  
  /**
   * Sets the ending student and ending coordinate of the arrow.
   * @param endingStudent student at the tip of the arrow
   * @param endX ending x position
   * @param endY ending y position
   */
  public void setEndingStudent(Student endingStudent, float endX, float endY) {
    this.endingStudent = endingStudent;
    ArrowCoordinate endingCoordinate = new ArrowCoordinate(endX, endY);
    coordinates.add(endingCoordinate);
  }
  
  @Override
  public String toString() {
    return this.startingStudent.getName() + " " + this.endingStudent.getName();
  }
  
  public String[] getStudentNames() {
    String[] studentNames = new String[2];
    studentNames[0] = startingStudent.getName();
    studentNames[1] = endingStudent.getName();
    return studentNames;
  }
  
  /**
   * Objects of the <b>ArrowCoordinate</b> class represents the total coordinates required for
   * an arrow (I.E, a starting XY coordinate, and an ending XY coordinate)
   */
  public static class ArrowCoordinate {
    //variables storing the starting x and y coordinates of a coordinate
    private float xCord;
    private float yCord;
    
    /**
     * Constructor for coordinate taking a starting x position and starting y position.
     * @param x x position
     * @param y  position
     */
    public ArrowCoordinate(float x, float y) {
      this.xCord = x;
      this.yCord = y;
    }
  
    public float getxCord() {
      return xCord;
    }
  
    public float getyCord() {
      return yCord;
    }
    
  }
}
