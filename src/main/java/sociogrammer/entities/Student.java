package sociogrammer.entities;

/**
 * Represents a student. Student objects also keep track of the interactions between this student
 * and other students.
 *
 * @author Lars Ivar Ramberg
 * @version 25.05.2020
 */
public class Student {
  
  private String name;
  
  /**
   * Constructor for student objects, taking a name for the student.
   * @param name name of the new student
   * @throws IllegalArgumentException if name is blank, empty, or null
   */
  public Student(String name) {
    if (!isValidName(name)) {
      throw new IllegalArgumentException("Name may not be blank or null");
    } else {
      this.name = name;
    }
  }
  
  /**
   * Returns the name of the student.
   * @return the name of the student
   */
  public String getName() {
    return name;
  }
  
  /**
   * Sets the name of the student according to the provided parameter.
   * @param name name to set.
   * @throws IllegalArgumentException if name is blank, empty, or null
   */
  public void setName(String name) {
    if (!isValidName(name)) {
      throw new IllegalArgumentException("Name may not be blank or null");
    } else {
      this.name = name;
    }
  }
  
  /**
   * Helper method for checking whether or not a name is valid.
   * @param name name to validate
   * @return <code>false</code> if the name is null, empty, or blank. Otherwise, <code>true</code>
   *      is returned
   */
  private boolean isValidName(String name) {
    boolean valid = true;
    if (name == null || name.isBlank() || name.isEmpty()) {
      valid = false;
    }
    return valid;
  }
  
  @Override
  public String toString() {
    return name;
  }
  
}
