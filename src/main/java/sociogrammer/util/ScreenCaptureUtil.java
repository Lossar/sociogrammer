package sociogrammer.util;

import java.io.File;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javax.imageio.ImageIO;
import javafx.embed.swing.SwingFXUtils;

/**
 * Utilities for screencapture.
 *
 * @author Lars Ivar Ramberg
 * @version 09.06.2020
 */
public class ScreenCaptureUtil {
  private ScreenCaptureUtil() {
    //Purposefully left empty for utility class
  }
  
  /**
   * Captures the current screen.
   */
  public static void captureScreen(Pane root, File file) {
    
    try {
      WritableImage image = root.snapshot(new SnapshotParameters(), null);
      ImageIO.write(SwingFXUtils.fromFXImage(image, null),
                    "png", file);
    } catch (Exception e) {
      System.out.println("As a wise man once said, that did not go well");
    }
  }
  
}
