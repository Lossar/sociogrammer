package sociogrammer.util;

import java.io.File;
import java.util.Optional;
import javafx.stage.FileChooser;
import sociogrammer.gui.App;

/**
 * The static file util class contains utilities to aid the user in accessing their files and
 * selecting files or folders.
 */
public class FileUtil {
  private FileUtil() {
    //Intentionally left empty
  }
  
  /**
   * Shows a dialog window for saving a file, and returns the new file if one is created.
   * @return the new file to be saved, or an empty optional if the saving is cancelled
   */
  public static Optional<File> doGetNewFileFromUser() {
    File file;
    FileChooser fc =  new FileChooser();
    fc.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("jpg", "*.jpg")
    );
    file = fc.showSaveDialog(App.getStage());
    return Optional.ofNullable(file);
  }
}
